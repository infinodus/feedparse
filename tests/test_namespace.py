import unittest
from feedparse.namespace import Namespace, Registry
from feedparse.namespace import BaseHandler, NamespaceHandler

class MockHandler(BaseHandler):
    def start(self, tag, namespace, attrib, context, **kwargs):
        pass

    def end(self, tag, namespace, data, context, **kwargs):
        pass

class MockNamespaceHandler(NamespaceHandler):
    def __init__(self):
        self._ns = []

    @property
    def namespaces(self):
        return self._ns

    def start(self, tag, namespace, attrib, context, **kwargs):
        pass

    def end(self, tag, namespace, data, context, **kwargs):
        pass

class NamespaceTest(unittest.TestCase):

    def setUp(self):
        self.namespace = Namespace('my', 'http://my.ns/1.1/')

    def test_register_handler(self):
        """Test register base handler
        """
        handler = MockHandler()
        registry = Registry()
        registry.register(handler)

        # no namespaces
        self.assertDictEqual(dict(registry), {})
        ns, handlers = registry.prefix('test')
        self.assertEqual(ns, None)
        self.assertIn(handler, handlers)

    def test_register_namespace_handler(self):
        """Test register namespace handler
        """
        handler = MockNamespaceHandler()
        handler.namespaces.append(self.namespace)

        registry = Registry()
        registry.register(handler)

        ns, handlers = registry.prefix(self.namespace.prefix)
        self.assertEqual(ns, self.namespace)
        self.assertIn(handler, handlers)

    def test_register_load_entrypoints(self):
        """Test register loads default entrypoint handlers
        """
        registry = Registry()
        registry.load()

        # default namespaces:
        # common, atom, media, dc, dcterm
        self.assertEqual(len(registry), 5)

    def test_register_get_prefix(self):
        """Test register get by prefix
        """
        handler = MockNamespaceHandler()
        handler.namespaces.append(self.namespace)

        registry = Registry()
        registry.register(handler)

        ns, handlers = registry.prefix('my')
        self.assertEqual(ns, self.namespace)
        self.assertIn(handler, handlers)

    def test_register_get_uri(self):
        """Test register get by uri
        """
        handler = MockNamespaceHandler()
        handler.namespaces.append(self.namespace)

        registry = Registry()
        registry.register(handler)

        ns, handlers = registry.uri('http://my.ns/1.1/')
        self.assertEqual(ns, self.namespace)
        self.assertIn(handler, handlers)

    def test_register_has_multiple_handlers(self):
        """Test register sets multiple handlers for namespace
        """
        namespace = Namespace('dc', 'http://purl.org/dc/elements/1.1/')
        handler = MockNamespaceHandler()
        handler.namespaces.append(namespace)

        registry = Registry()
        registry.register(handler)

        ns, handlers = registry.uri('http://purl.org/dc/elements/1.1/')
        self.assertEqual(len(handlers), 2)
        self.assertIn(handler, handlers)

if __name__ == '__main__':
    unittest.main()
