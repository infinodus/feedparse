import os
import unittest
from feedparse import Parser
from feedparse import Channel, Item
from datetime import datetime, timezone

class TestParse(unittest.TestCase):
    def setUp(self):
        self.base = os.path.dirname(__file__)

    def test_parse_rss20(self):
        """Test basic RSS 2.0
        """
        fname = os.path.join(self.base, 'data/feed_rss20.xml')
        channel = None
        items = []

        parser = Parser()
        with open(fname, 'rb') as reader:
            for res in parser.parse(reader):
                if isinstance(res, Channel):
                    channel = res
                else:
                    items.append(res)

        self.assertIsNotNone(channel)
        self.assertEqual(len(items), 1)

        # channel basic fields extracted
        fields = ['description', 'link', 'version', 'title', 'language']
        self.assertCountEqual(fields, list(channel.keys()))
        # channel language conversion
        self.assertEqual(channel['language'], 'en_US')

        # item basic fields extracted
        fields = ['category', 'published', 'links',
                  'description', 'guid', 'media']
        self.assertCountEqual(fields, list(items[0].keys()))
        # item date conversion
        result = datetime(2016, 12, 6, 1, 56, 2, tzinfo=timezone.utc)
        self.assertEqual(items[0]['published'], result)

    def test_parse_atom10(self):
        """Test basic Atom 2.0
        """
        fname = os.path.join(self.base, 'data/feed_atom10.xml')
        channel = None
        items = []

        parser = Parser()
        with open(fname, 'rb') as reader:
            for res in parser.parse(reader):
                if isinstance(res, Channel):
                    channel = res
                else:
                    items.append(res)

        self.assertIsNotNone(channel)
        self.assertEqual(len(items), 1)

        # channel basic fields extracted
        fields = ['updated', 'id', 'link', 'version', 'title']
        self.assertCountEqual(fields, list(channel.keys()))
        # channel date conversion
        result = datetime(2016, 12, 6, 18, 30, 2, tzinfo=timezone.utc)
        self.assertEqual(channel['updated'], result)

        # item date conversion
        result = datetime(2016, 12, 6, 18, 30, 2, tzinfo=timezone.utc)
        self.assertEqual(items[0]['updated'], result)
        # items parsed links
        result = [
            {
                'url': 'http://example.org/2003/12/13/atom03.html',
                'rel': 'alternate', 'type': 'text/html'
            },
            {
                'url': 'http://example.org/2003/12/13/atom03/edit',
                'rel': 'edit'
            }
        ]
        self.assertCountEqual(items[0]['links'], result)
