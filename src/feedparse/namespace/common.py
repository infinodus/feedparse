from . import Namespace
from .abc import NamespaceHandler
from feedparse.util import to_datetime, map_attrib, parse_lang


class Handler(NamespaceHandler):
    namespaces = [
        Namespace(None, None)
    ]

    def __init__(self):
        self._elems = {}

    def start(self, tag, namespace, attrib, context, **kwargs):
        if tag == 'category':
            if 'category' not in context:
                context['category'] = []
            self._elems['category'] = attrib.copy()

        if tag == 'guid':
            self._elems[tag] = attrib.copy()

        # see media handler
        if tag == 'enclosure':
            data = map_attrib(attrib)
            if 'enclosure' not in context['media']:
                context['media']['enclosure'] = [data]
            else:
                context['media']['enclosure'].append(data)

        if tag == 'image':
            self._elems['logo'] = {}

    def end(self, tag, namespace, data, context, **kwargs):
        if tag in ['id', 'link', 'title', 'copyright'] and tag not in context:
            self._elems[tag] = data
            self._flush(tag, context)

        if tag == 'category':
            self._elems[tag]['term'] = data
            self._flush(tag, context)

        if tag == ['updated'] and tag not in context:
            self._elems[tag] = to_datetime(data)
            self._flush(tag, context)

        if tag in ['lastBuildDate', 'pubDate', 'date'] \
                and 'published' not in context:
            try:
                self._elems['published'] = to_datetime(data)
                self._flush('published', context)
            except ValueError:
                pass

        if tag in ['description'] and 'description' not in context:
            self._elems['description'] = data
            self._flush('description', context)

        if tag == 'guid':
            value = self._elems.pop(tag)
            if value.get('isPermaLink', 'false').lower() in ['true'] \
                    and not context.get('link'):
                context['link'] = data
            context['guid'] = data

        if tag == 'language' and tag not in context:
            self._elems[tag] = parse_lang(data)
            self._flush(tag, context)

        if tag == 'image':
            self._elems['logo'] = self._elems['logo'].get('url')
            self._flush('logo', context)

        if tag == 'url':
            self._elems[tag] = data
            self._flush(tag, context, parents=['logo'])
