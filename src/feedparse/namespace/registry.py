import logging
from lxml import etree
from functools import lru_cache
from typing import Callable, Tuple, Sequence
from .abc import BaseHandler, NamespaceHandler
from pkg_resources import (iter_entry_points, DistributionNotFound)
from collections import UserDict, MutableMapping, namedtuple, defaultdict

logger = logging.getLogger(__name__)


class Namespace(namedtuple('Namespace', ['prefix', 'uri'])):
    """A namespace definition
    """


class Registry(UserDict):
    """A namespace registry
    """
    def __init__(self, initial: dict = None):
        super().__init__()
        self._loaded = False
        self.data = defaultdict(list)
        self.anon = set()
        if initial and isinstance(initial, MutableMapping):
            self.update(initial)

    def __setitem__(self, namespace: Namespace, handler: BaseHandler) -> None:
        """Set a namespace, handler pair
        """
        self.data[namespace].append(handler)

    @classmethod
    @lru_cache(maxsize=None)
    def parse_tag(cls, tag: str) -> Tuple[str, str]:
        """Parse a tag in uri and tag name components
        """
        qname = etree.QName(tag)
        return qname.namespace, qname.localname

    def load(self) -> None:
        """Load entry points
        """
        if self._loaded:
            return

        for entrypoint in iter_entry_points('feedparse.handlers'):
            try:
                handler = entrypoint.load()()
                self.register(handler)
            except DistributionNotFound:
                logger.warning(f'handler {entrypoint.name} not found')

        self._loaded = True

    def register(self, handler: BaseHandler) -> None:
        """Register a handler
        """
        if isinstance(handler, NamespaceHandler):
            for namespace in handler.namespaces:
                self[namespace] = handler
        elif isinstance(handler, BaseHandler):
            self.anon.add(handler)
        else:
            raise TypeError('handler should be of class BaseHandler')

    @lru_cache(maxsize=None)
    def prefix(self, prefix: str) -> Tuple[Namespace, Sequence[BaseHandler]]:
        """Get namespace and handlers by prefix
        """
        return self._find(lambda namespace: namespace.prefix == prefix)

    @lru_cache(maxsize=None)
    def uri(self, uri: str) -> Tuple[Namespace, Sequence[BaseHandler]]:
        """Get namespace and handlers by uri
        """
        return self._find(lambda namespace: namespace.uri == uri)

    def _find(self, f: Callable) -> Tuple[Namespace, Sequence[BaseHandler]]:
        if not self._loaded:
            self.load()

        for namespace in self.keys():
            if f(namespace):
                return namespace, self.get(namespace)

        return None, self.anon

    def __hash__(self):
        return id(self)
