from . import Namespace
from .abc import NamespaceHandler
from feedparse.util import to_datetime


class Handler(NamespaceHandler):
    namespaces = [
        Namespace('atom', 'http://www.w3.org/2005/Atom')
    ]

    def __init__(self):
        self._elems = {}

    def start(self, tag, namespace, attrib, context, **kwargs):
        if tag == 'link':
            if not attrib.get('rel') or attrib.get('rel') == 'self':
                context['link'] = attrib.get('href')
            elif attrib.get('rel') == 'enclosure':
                data = attrib.copy()
                del data['rel']
                if 'enclosure' not in context['media']:
                    context['media']['enclosure'] = [data]
                else:
                    context['media']['enclosure'].append(data)
            else:
                if 'links' not in context:
                    context['links'] = []
                data = attrib.copy()
                data['url'] = data.pop('href')
                context['links'].append(data)

        # parent is context
        if tag in ['author', 'contributor']:
            if tag not in context:
                context[tag] = []
            self._elems[tag] = {}

        if tag == 'category':
            self._elems[tag] = attrib.copy()

    def end(self, tag, namespace, data, context, **kwargs):
        if tag == 'title' and context.get(tag) is None:
            context[tag] = data

        if tag == 'summary' and 'description' not in context:
            context['description'] = data

        if tag in ['id', 'rights', 'logo']:
            context[tag] = data

        if tag in ['updated', 'published']:
            context[tag] = to_datetime(data)

        if tag in ['name', 'uri', 'email']:
            self._elems[tag] = data
            self._flush(tag, context, parents=['author', 'contributor'])

        if tag in ['author', 'contributor']:
            self._flush(tag, context)

        if tag == 'category':
            self._flush(tag, context)
