from . import Namespace
from .abc import NamespaceHandler
from feedparse.util import to_datetime, parse_lang


class Handler(NamespaceHandler):
    namespaces = [
        Namespace('dc', 'http://purl.org/dc/elements/1.1/'),
        Namespace('dcterms', 'http://purl.org/dc/terms/')
    ]

    def __init__(self):
        self._elems = {}

    def start(self, tag, namespace, attrib, context, **kwargs):
        if tag == 'creator' and tag not in context:
            context['author'] = []

        if tag == 'contributor' and tag not in context:
            context[tag] = []

    def end(self, tag, namespace, data, context, **kwargs):
        if tag == 'title' and tag not in context:
            self._elems[tag] = data
            self._flush(tag, context)

        if tag == 'description' and 'description' not in context:
            context['description']

        if tag == 'date' and 'published' not in context:
            context['published'] = to_datetime(data)

        if tag == 'rights' and tag not in context:
            context['rights'] = data

        if tag == 'creator':
            if any([a['name'].lower() == data.lower()
                    for a in context.get('author')]):
                return
            context['author'].append({'name': data})

        if tag == 'contributor':
            if any([a['name'].lower() == data.lower()
                    for a in context.get('contributor')]):
                return
            context['contributor'].append({'name': data})

        if tag == 'language' and tag not in context:
            self._elems[tag] = parse_lang(data)
            self._flush(tag, context)

        if tag == 'subject' and data:
            context['category'].append({'term': data})
