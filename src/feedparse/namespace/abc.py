import abc
from typing import Sequence


class BaseHandler(metaclass=abc.ABCMeta):
    """Base parser target
    """

    @abc.abstractmethod
    def start(self, tag: str, namespace: 'feedparse.namespace.Namespace',
              attrib: dict, context: 'feedparse.events.ParseResult', **kwargs):
        """Handle a new event
        """

    @abc.abstractmethod
    def end(self, tag: str, namespace: 'feedparse.namespace.Namespace',
            data: str, context: 'feedparse.events.ParseResult', **kwargs):
        """Close the current element
        """

    def _flush(self, tag: str, context: 'feedparse.events.ParseResult',
               parents: list = None):
        """Flush child element values to appropriate parents
        """
        if tag not in self._elems:
            return

        data = self._elems.pop(tag)
        if not data:
            return

        # in parents
        parents = parents or []
        for parent in parents:
            if parent in self._elems:
                if isinstance(self._elems[parent], dict):
                    self._elems[parent][tag] = data
                if isinstance(self._elems[parent], list):
                    self._elems[parent].append(data)
                return

        if isinstance(context.get(tag), dict):
            context[tag] = data
        if isinstance(context.get(tag), list):
            context[tag].append(data)
        if tag not in context:
            context[tag] = data


class NamespaceHandler(BaseHandler):
    """Namespaced parser target
    """

    @abc.abstractproperty
    def namespaces(self) -> Sequence['feedparse.namespace.Namespace']:
        """A listing of namespaces for this handlers
        """
