from . import Namespace
from .abc import NamespaceHandler
from feedparse.util import map_attrib


class Handler(NamespaceHandler):
    namespaces = [
        Namespace('media', 'http://search.yahoo.com/mrss/')
    ]

    def __init__(self):
        self._elems = {}

    def start(self, tag, namespace, attrib, context, **kwargs):
        # <media:content> is a sub-element of either <item> or <media:group>.
        # Media objects that are not the same content should not be included
        # in the same <media:group> element. The sequence of these items
        # implies the order of presentation.

        if tag == 'content':
            allowed = ['url', 'fileSize', 'type', 'medium', 'isDefault',
                       'expression', 'bitrate', 'framerate', 'samplingrate',
                       'channels', 'duration', 'height', 'width', 'lang']

            data = map_attrib(attrib, allowed)

            if tag not in context['media']:
                context['media'][tag] = [data]
            else:
                context['media'][tag].append(data)

        # The following elements are optional and may appear as sub-elements
        # of <channel>, <item>, <media:content> and/or <media:group>.

        # Only elements with attributes on start

        if tag == 'thumbnail':
            context['media'][tag] = map_attrib(attrib)

        if tag in ['category', 'player']:
            self._elems[tag] = map_attrib(attrib)

        if tag in ['credit', 'restriction', 'copyright']:
            self._elems[tag] = map_attrib(attrib)

        if tag == 'community':
            context['media'][tag] = {}
            self._elems[tag] = {}

        if tag in ['starRating', 'statistics', 'tags']:
            self._elems[tag] = map_attrib(attrib)

    def end(self, tag, namespace, data, context, **kwargs):
        if tag == 'description' and 'description' not in context:
            context['description'] = data

        if tag == 'title' and tag not in context:
            self._elems[tag] = data
            self._flush(tag, context)

        if tag == 'category' and data:
            self._elems[tag] = {'term': data}
            self._flush(tag, context)

        if tag == 'restriction':
            attrs = self._elems.pop(tag, {})
            attrs['value'] = [v.lower() for v in data.split()]

            context['media'][tag] = attrs

        if tag in ['starRating', 'statistics', 'tags']:
            self._flush(tag, context, parents=['community'])

        if tag == 'community':
            context['media'][tag].update(self._elems[tag])
