import re
from feedparse.namespace import Namespace
from feedparse.namespace.abc import NamespaceHandler


class GoogleHandler(NamespaceHandler):
    """Google Shopping Product data
    """
    namespaces = [
        Namespace('google', 'http://base.google.com/ns/1.0')
    ]

    RE_PRICE = re.compile(r'(?P<value>[\d\.\,]+)\s?(?P<currency>[A-Z]{0,3})')

    _in_shipping = False

    def __init__(self):
        self._elems = {}

    def start(self, tag, namespace, attrib, context, **kwargs):
        if tag == 'shipping':
            self._in_shipping = True
            context.setdefault(tag, [])
            context[tag].append({})

        if tag in ['additional_image_link', 'google_product_category'] \
                and tag not in context:
            context[tag] = []

    def end(self, tag, namespace, data, context, **kwargs):
        if tag in ['id', 'title', 'mpn', 'brand',
                   'image_link', 'availability', 'condition'] \
                and tag not in context:
            self._elems[tag] = data

        if tag == 'price':
            if self._in_shipping:
                context['shipping'][-1][tag] = self.format_price(data)
            else:
                self._elems[tag] = self.format_price(data)

        if tag in ['country', 'service'] and self._in_shipping:
            context['shipping'][-1][tag] = data

        if tag == 'sale_price':
            self._elems[tag] = self.format_price(data)

        if tag in ['additional_image_link']:
            context[tag].append(data)

        if tag in ['google_product_category']:
            context[tag].append(data)

        if tag == 'shipping':
            self._in_shipping = False

        self._flush(tag, context)

    def format_price(self, value):
        match = self.RE_PRICE.match(value)
        if match:
            return {
                'value': float(match.group('value').replace(',', '.')),
                'currency': match.group('currency') or 'EUR'
            }

        return value
