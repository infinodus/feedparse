__all__ = ['BaseParser', 'Parser', 'AsyncParser']

import abc
import logging
from .namespace import Registry
from .exceptions import ParseError, FeedParseError
from .events import XMLPullParser, EventHandler, ParseItem, Item, Channel
from typing import Iterator, Iterable, AsyncIterator, AsyncIterable, Union

logger = logging.getLogger(__name__)


class BaseParser(metaclass=abc.ABCMeta):
    def __init__(self, registry: Registry = None):
        self.registry = registry \
            if isinstance(registry, Registry) else Registry()

    def producer(self, parser: XMLPullParser,
                 data: Iterable) -> Iterator[None]:
        """Produce parsed entries
        """
        for chunk in data:
            yield parser.feed(chunk)

        try:
            yield parser.close()  # close + any left
        except Exception:
            logger.exception('error on parser close')

    def consumer(self, parser: XMLPullParser,
                 data: Iterable) -> Iterator[ParseItem]:
        """Consume parsed entries
        """
        for _ in self.producer(parser, data):
            for action, result in parser.read_events():
                if result and action == 'end' and result.value:
                    yield result
                    result.node.clear()  # release

    def get_parser(self, handler: EventHandler = None) -> XMLPullParser:
        """Return a parser
        """
        handler = handler \
            if isinstance(handler, EventHandler) \
            else EventHandler(registry=self.registry)

        return XMLPullParser(events=['end'], target=handler)

    @abc.abstractmethod
    def parse(self, data: Iterable,
              handler: EventHandler = None,
              **kwargs) -> Iterator[Union[Channel, Item]]:
        """Parse data
        """


class Parser(BaseParser):
    """Synchronous feed parser
    """
    def parse(self, data: Iterable,
              handler: EventHandler = None,
              **kwargs) -> Iterator[Union[Channel, Item]]:
        """Parse a feed

        import requests

        parser = Parser()
        for item in parser.parse(requests.get(url).iter_lines()):
            print(item)
        """
        parser = self.get_parser(handler)
        try:
            for item in self.consumer(parser, data):
                yield item.value
        except ParseError:
            logger.exception('parser error')
            raise FeedParseError('feed syntax error')
        except FeedParseError:
            logger.exception('handler error')
            raise


class AsyncParser(BaseParser):
    """Asynchronous feed parser
    """
    async def producer(self, parser: XMLPullParser,
                       data: AsyncIterable) -> AsyncIterator[None]:
        while not data.at_eof():
            chunk = await data.read(2048)
            yield parser.feed(chunk)

        try:
            yield parser.close()  # close + any left
        except Exception:
            logger.exception('error on parser close')

    async def consumer(self, parser: XMLPullParser,
                       data: AsyncIterable) -> AsyncIterator[ParseItem]:
        """Consume parsed entries
        """
        async for _ in self.producer(parser, data):
            for action, result in parser.read_events():
                if result and action == 'end' and result.value:
                    yield result
                    result.node.clear()  # release

    async def parse(self, data: Iterable,
                    handler: EventHandler = None,
                    **kwargs) -> AsyncIterator[Union[Channel, Item]]:
        """Parse a feed

        import aiohttp

        parser = AsyncParser()
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                async for item in parser.parse(resp.content):
                    print(item)
        """
        parser = self.get_parser(handler)
        try:
            async for item in self.consumer(parser, data):
                yield item.value
        except ParseError:
            logger.exception('parser error')
            raise FeedParseError('feed syntax error')
        except FeedParseError:
            logger.exception('handler error')
            raise
