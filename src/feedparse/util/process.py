import uuid
from typing import Union
from datetime import datetime, timezone
from urllib.parse import urlparse, urljoin
from feedparse.events import Channel, Item

class PostProcessor:
    """Post-process parsed items
    """
    def __init__(self, url: str, published: datetime=None, encoding='utf8'):
        self.handlers = {}
        self.link = url
        self.published = published or datetime.now(timezone.utc)

    def prepare_link(self, item: Union[Channel, Item]):
        """Prepare and normalize a url
        """
        if 'link' in item:
            url = urlparse(item['link'])
            if not url.scheme or url.netloc:
                item['link'] = urljoin(self.link, item['link'])

    def prepare_identifier(self, item: Union[Channel, Item]):
        """Prepare and set an identifier

        Don't trust guid, set an identifier based on either url or a combination of title and description. The value is a UUID from the SHA-1 hash of a URL namespace UUID and identifier.
        """
        ident = ''

        if 'link' in item:
            ident = item['link']
        else:
            if 'title' in result:
                ident += result['title']
            if 'description' in result:
                ident += result['description']

        if ident != '':
            item['_id'] = uuid.uuid5(uuid.NAMESPACE_URL, ident)

    def prepare_published(self, item: Item):
        """Prepare and set a published date
        """
        if isinstance(item, Item):
            item['_published'] = \
                item.get('published', self.published)

    def prepare(self, item: Union[Channel, Item], actions=['link', 'identifier', 'published']):
        """Prepare an item
        """
        for action in actions:
            handler = self.handlers.setdefault(
                action, getattr(
                    self, 'prepare_{}'.format(action), lambda x:x))
            handler(item)

    def __call__(self, item: Item, **kwargs):
        self.prepare(item, **kwargs)
