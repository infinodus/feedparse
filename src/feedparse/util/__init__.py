import re
import time
import logging
import datetime
from dateutil import parser
from collections import OrderedDict

logger = logging.getLogger(__name__)

parse_time = lambda v:parser.parse(v).time()

# mainly rss media attributes
ATTRIB_MAP = {
    'filesize': int,
    'isdefault': lambda x: x.lower() in ['true'],
    'bitrate': int,
    'framerate': int,
    'samplingrate': float,
    'channels': int,
    'duration': int,
    'height': int,
    'width': int,
    'time': parse_time,
    'start': parse_time,
    'end': parse_time,
    'count': int,
    'min': int,
    'max': int,
    'average': float,
    'views': int,
    'favorites': int
}

def parse_lang(data):
    result = OrderedDict(zip(
        ['lang', 'region'], data.replace('-', '_').split('_')))

    if result.get('region'):
        result['region'] = result['region'].upper()
    
    return '_'.join(list(result.values()))

def map_attrib(attrib, allowed=[]):
    """Map common attributes to their types
    
    Optionally skip non allowed keys.
    """
    data = {}
    for key, value in attrib.items():
        if allowed and key not in allowed:
            continue
        if key.lower() in ATTRIB_MAP:
            data[key] = ATTRIB_MAP[key.lower()](value)
        else:
            data[key] = value
    return data

def to_datetime(value):
    """Cast to datetime

    :param value: value to format
    :type value: str
    """
    if not isinstance(value, datetime.datetime):
        try:
            return parser.parse(value)
        except (AttributeError, ValueError):
            raise ValueError(
                '{!r} is not a valid datetime representation'.format(value))
    return value

def get_cleaner(encoding='utf-8'):
    """Return a ftfy text cleaner if installed
    """
    try:
        from ftfy import fix_text
        return lambda x: fix_text(x.decode(encoding))
    except ImportError:
        logger.warning('ftfy not installed')
        return lambda x:x
