import lxml
import logging
import contextlib
from lxml import etree
from html import unescape
from typing import NamedTuple, Union
from lxml.etree import XMLPullParser
from .exceptions import FeedParseError
from .namespace import Namespace, Registry
from collections import Mapping, MutableMapping

logger = logging.getLogger(__name__)


class ParseResult(dict, MutableMapping):
    """A parsed element result
    """


class Channel(ParseResult):
    """A feed channel element
    """


class Item(ParseResult):
    """A feed item element
    """
    def __init__(self, data: dict = None):
        super().__init__()
        defaults = {
            'category': [],
            'links': [],
            'media': {}
        }
        defaults.update(data or {})
        self.update(defaults)


class ParseItem(NamedTuple):
    """A parsed node item result
    """
    node: lxml.etree._Element
    value: Union[Channel, Item]


class Version(NamedTuple):
    """Feed version information
    """
    scheme: str
    version: str


class EventHandler(etree.TreeBuilder):
    """Parser target that builds a tree
    """
    def __init__(self, *args, registry: Registry = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.registry = registry
        self.feed = Channel()
        self.context = None
        self.namespace = None
        self.mapping = {}

    def start(self, tag: str, attrib: dict, nsmap: Mapping = None):
        """Opens a new element
        """
        uri, name = self.registry.parse_tag(tag)

        if not self.feed.get('version') and name in ['rss', 'feed']:
            if name == 'rss':
                self.feed['version'] = \
                    Version('rss', attrib.get('version', '2.0.1'))
            if name == 'feed':
                self.feed['version'] = \
                    Version('atom', attrib.get('version', '1.0'))

            self.namespace, _ = self.registry.prefix(uri)

        if nsmap:
            self.mapping.update({u: Namespace(p, u) for p, u in nsmap.items()})

        if name in ['entry', 'item']:
            self.context = Item()

        # although we feed it bytes, lxml sets data as string
        self._data = ''

        if uri:
            namespace, handlers = self.registry.uri(uri)
        else:
            if not self.namespace:
                raise FeedParseError(
                    'invalid feed format, no namespace detected')
            namespace, handlers = self.registry.uri(self.namespace.uri)

        context = self.context if self.context is not None else self.feed

        if not namespace and handlers:
            # namespace from mapping
            namespace = self.mapping.get(uri)

        for handler in handlers:
            try:
                handler.start(name, namespace, dict(attrib),
                              context, mapping=self.mapping)
            except Exception:
                logger.exception('handler error')

        return super().start(tag, attrib)

    def data(self, data: str):
        """Adds text to the current element
        """
        self._data += unescape(data)
        return super().data(data)

    def end(self, tag: str):
        """Closes the current element
        """
        uri, name = self.registry.parse_tag(tag)

        if uri:
            namespace, handlers = self.registry.uri(uri)
        else:
            namespace, handlers = self.registry.uri(self.namespace.uri)

        context = self.context if self.context is not None else self.feed

        if not namespace and handlers:
            # namespace from mapping
            namespace = self.mapping.get(uri)

        for handler in handlers:
            handler.end(name, namespace, self._data,
                        context, mapping=self.mapping)

        value = None
        if name in ['channel', 'feed']:
            value = self.feed
        if name in ['entry', 'item']:
            value = self.context
            self.context = None

        return ParseItem(super().end(tag), value)

    def comment(self, comment: str):
        """Handle a comment
        """

    def pi(self, target, data):
        """...
        """

    def close(self):
        """Flushes builder buffers, returns the toplevel document element
        """
        with contextlib.suppress(Exception):
            return super().close()
