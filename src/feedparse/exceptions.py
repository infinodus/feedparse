from lxml import etree


class ParseError(etree.ParseError):
    """Parser error
    """


class FeedError(Exception):
    """Base feed parser error
    """


class FeedParseError(FeedError):
    """Invalid feed format error
    """
