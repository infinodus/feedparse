from setuptools import setup, find_packages

install_requires = ['lxml', 'python-dateutil']

dependency_links = []

tests_require = ['aiohttp', 'requests']

entry_points = {
    'feedparse.handlers': [
        'dc = feedparse.namespace.dc:Handler',
        'atom = feedparse.namespace.atom:Handler',
        'media = feedparse.namespace.media:Handler',
        'common = feedparse.namespace.common:Handler'
    ]
}

extras_require = {
    'ftfy': ['ftfy>=4.0.0']
}

setup(
    name='feedparse',
    version='0.2.0',
    description='feedparse 2',
    author="Misja Hoebe",
    author_email="misja.hoebe@gmail.com",
    url="http://none",
    packages=find_packages(
        'src', exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    package_dir={'': 'src'},
    install_requires=install_requires,
    dependency_links=dependency_links,
    test_suite='tests',
    tests_require=tests_require,
    extras_require=extras_require,
    entry_points=entry_points
)
